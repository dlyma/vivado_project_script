#!/bin/bash

#Description: This scripts builds a vivado project using your source files
#Your folder structure must be set to this:
#├── build.tcl
#└── srcs
    #├── bd
    #├── constraints
    #├── ip
    #│   ├── config
    #│   ├── dcp
    #│   └── repo
    #└── rtl
mkdir srcs/
mkdir srcs/bd
mkdir srcs/constraints/
mkdir srcs/ip/
mkdir srcs/ip/config
mkdir srcs/ip/dcp
mkdir srcs/ip/repo
mkdir srcs/rtl

